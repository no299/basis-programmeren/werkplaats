package nl.novi.basisprogrammeren;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        Rooster langRooster = new Rooster();
        langRooster.setUren(10L);

        Werkplaats werkplaatsAmsterdam = new Werkplaats();

        Monteur monteur1 = new Monteur();
        monteur1.setNaam("Henk");
        monteur1.setId(1L);
        monteur1.setVerjaardag(LocalDate.of(1989, 1, 4));
        monteur1.setRooster(langRooster);
        monteur1.updateId();

        Monteur monteur2 = new Monteur();
        monteur2.setNaam("Piet");
        monteur2.setVerjaardag(LocalDate.now());
        monteur2.setRooster(langRooster);
        monteur2.setId(2L);

        Monteur monteur3 = new Monteur();
        monteur3.setNaam("Bert");

        Werkplaats werkplaatsAlmere = new Werkplaats();

        Monteur monteur4 = new Monteur();
        monteur4.setNaam("Klaas1");

        Monteur monteur5 = new Monteur();
        monteur5.setNaam("Klaas2");

        Monteur monteur6 = new Monteur();
        monteur6.setNaam("Klaas3");

        List<Monteur> monteurs2 = List.of(monteur1, monteur4, monteur5, monteur6);
        werkplaatsAlmere.setMonteurs(monteurs2);

        System.out.println(werkplaatsAmsterdam);
    }
}
