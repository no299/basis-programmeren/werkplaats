package nl.novi.basisprogrammeren;

import java.time.LocalDate;

public class Monteur {

    private Long id;
    private String naam;
    private Rooster rooster;
    private LocalDate verjaardag;

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void updateId(){
        this.id++;
    }

    public Rooster getRooster() {
        return rooster;
    }

    public void setRooster(Rooster rooster) {
        this.rooster = rooster;
    }

    public LocalDate getVerjaardag() {
        return verjaardag;
    }

    public void setVerjaardag(LocalDate verjaardag) {
        this.verjaardag = verjaardag;
    }

    public boolean isJarig(){
        return verjaardag.isEqual(LocalDate.now());
    }
}
