package nl.novi.basisprogrammeren;

public class Rooster {

    private Long uren;

    public Long getUren() {
        return uren;
    }

    public void setUren(Long uren) {
        this.uren = uren;
    }
}
