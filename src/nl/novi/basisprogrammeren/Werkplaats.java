package nl.novi.basisprogrammeren;

import java.util.List;

public class Werkplaats {

    private List<Monteur> monteurs;

    public List<Monteur> getMonteurs() {
        return monteurs;
    }

    public void setMonteurs(List<Monteur> monteurs) {
        this.monteurs = monteurs;
    }
}
